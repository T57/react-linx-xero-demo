# README #

## Overview ##

This application demonstrates the signup of users and connecting to their Xero accounts using a React SPA with a Linx API. No further functionality is implemented but it will be quite straighforward to add more Xero integration as we have the authentication done.

The application has two parts:

* UI: React application with 
    * Signup
    * Login
    * Forgot password
    * External API
    * Connect to Xero using OAuth2

* API: Linx application that
    * Hosts the API
    * Saves data to database
    * Issues JWT tokens
    * Connects to Xero using OAuth2
    * Encrypts sensitive data

## Getting started ##

### Xero ###

For the Xero bits to work you'll need a Xero ClientId and ClientSecret. See https://developer.xero.com/documentation/getting-started/getting-started-guide for further information.

### Database ###

The sql scripts and following instructions targets MS SQL Server. You can substitute it with MySQL or PostgreSQL by changing the scripts and fixing the Linx solution - just debug the processes in the Db project to find out where there are incompatible SQL statements.

1. Create a SQL Server database.
1. Open the Linx solution (api/LinxAPI.lsoz) in Linx Designer. If you don't have Linx download it [here](https://linx.software).
1. Run CreateTables under the Installer project in the debugger. Set SourceFolder to [repofolder]\api\db and DbConnection to the connectionstring to your database. Alternatively you can run the scripts in [repofolder]\api\db manually.

### API ###

1. Install Linx Server.
1. Deploy the Linx solution (api/LinxAPI.lsoz) to Linx Server either by deploying from Linx Designer or using the Linx Server UI.
1. Once deployed, go to the solution in Linx Server and make sure all your Settings are correct.
1. Start the API service.

### UI ###

1. Change REACT_APP_API_URI in .env.development to point to your Linx API. It should be the same value as API_BaseURI in Settings on Linx Server.
1. Use 'npm run start' to start the dev server.