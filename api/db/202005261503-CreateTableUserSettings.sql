CREATE TABLE [dbo].[UserSettings](
	[UserId] [int] NOT NULL,
	[XeroAccessToken] [nvarchar](max) NULL,
	[XeroRefreshToken] [nvarchar](max) NULL,
	[XeroTenant] [nvarchar](max) NULL,
	[XeroAccessTokenExpiry] [datetime] NULL,
 CONSTRAINT [PK_UserSettings] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]