import React from "react";
import NavBar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import {Link} from "react-router-dom";
import {useAPI} from "../common/apiContext";

export default function Header(){
    const api = useAPI();
    return (
        <NavBar variant="dark" bg="dark" expand="sm">
            <NavBar.Brand as={Link} to="/">SPA (React) + API (Linx) + OAuth2 (Xero)</NavBar.Brand>
            {api.isAuthenticated
            ?
            <Nav className="ml-auto" variant="pills">
                <Nav.Item>
                    <Nav.Link as={Link} to="/dashboard">Dashboard</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                    <Nav.Link as={Link} to="/settings">Settings</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                    <Nav.Link as={Link} to="/account">Account</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                    <Nav.Link as={Link} to="/?logout">Logout</Nav.Link>
                </Nav.Item>
            </Nav>
            :
            <Nav className="ml-auto" variant="pills">
                <Nav.Item>
                    <Nav.Link as={Link} to="/signup">Signup</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                    <Nav.Link as={Link} to="/login">Login</Nav.Link>
                </Nav.Item>
            </Nav>
            }
        </NavBar>
    );
}