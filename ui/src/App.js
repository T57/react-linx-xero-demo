import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect
  } from "react-router-dom";
import Signup from "./pages/Signup";
import Login from "./pages/Login";
import ConfirmSignup from "./pages/ConfirmSignup";
import Account from "./pages/Account";
import Container from "react-bootstrap/Container";
import Home from "./pages/Home";
import Header from "./components/Header";
import {useAPI} from "./common/apiContext";
import Dashboard from './pages/Dashboard';
import Settings from "./pages/Settings";
import XeroRedirect from "./pages/XeroRedirect";
import ForgotPassword from "./pages/ForgotPassword";
import ResetPassword from "./pages/ResetPassword";

export default function App(){
    const api = useAPI();

    function PrivateRoute({ children, ...rest }) {
        return (
          <Route
            {...rest}
            render={({ location }) =>
              api.isAuthenticated? (
                children
              ) : (
                <Redirect
                  to={{
                    pathname: "/login",
                    state: { from: location }
                  }}
                />
              )
            }
          />
        );
      }

    return (
        <Router>
            <Container>
            <Header></Header>
            <Switch>
                        <Route exact path="/">
                            <Home></Home>
                        </Route> 
                        <Route exact path="/login">
                            <Login/>
                        </Route>
                        <Route exact path="/signup">
                            <Signup/>
                        </Route>
                        <Route path="/confirm-signup/:code">
                            <ConfirmSignup/>
                        </Route>
                        <Route exact path="/?logout">
                            <Home/>
                        </Route>
                        <Route exact path="/forgot-password">
                            <ForgotPassword/>
                        </Route>
                        <Route path="/reset-password/:code">
                            <ResetPassword/>
                        </Route>
                        <PrivateRoute exact path="/account">
                            <Account/>
                        </PrivateRoute>
                        <PrivateRoute exact path="/dashboard">
                            <Dashboard/>
                        </PrivateRoute>
                        <PrivateRoute exact path="/settings">
                            <Settings/>
                        </PrivateRoute>
                        <PrivateRoute exact path="/auth/xero">
                            <XeroRedirect/>
                        </PrivateRoute>
                    </Switch>
            </Container>
        </Router>
    )
}