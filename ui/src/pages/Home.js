import React, {useEffect} from "react";
import Jumbotron from "react-bootstrap/Jumbotron";
import Button from "react-bootstrap/Button";
import {Link, useLocation, useHistory} from "react-router-dom";
import {useAPI} from "../common/apiContext";

export default function Home(){
    const api = useAPI();
    const location = useLocation();
    const history = useHistory();

    useEffect(()=>{
        const params = new URLSearchParams(location.search);
        if(params.get("logout") != null)
        {
            api.logout();
            history.push("/");
        }
    },[location, history, api]);

    return (
        <Jumbotron className="text-center">
            <h2>Use React with a Linx back-end</h2>
            <br/>
            <p>This is a sample to show how we can use a low code tool like Linx <br/>
            as the back-end for a SPA, in this case built with React.</p>
            <br/>
            <h3>How it works</h3>
            <br/>
            <p>The React application uses a REST API to provide functionality. It can<br/>
            be hosted anywhere (AWS, Azure, Firebase, Netlify etc).<br/>
            Linx Server hosts the REST API and provides the endpoints to <br/>
            signup users and integrate with Xero.</p>
            <br/>
            <h3>Why do it this way?</h3>
            <br/>
            <p>Because implementing back-ends with Linx is a lot<br/>
            easier than writing the code from scratch.</p>
            <br/>
            <Link to="/signup">
                <Button>Signup</Button>
            </Link>
        </Jumbotron>
    );
}