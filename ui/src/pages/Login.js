import React from "react";
import {useAPI} from "../common/apiContext";
import {useHistory, Link} from "react-router-dom";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import {Formik} from "formik";
import * as yup from "yup";

export default function Login() {
    const history = useHistory();
    const api = useAPI();

    const validationSchema = yup.object().shape({
        email: yup.string()
            .email("*Must be a valid email address")
            .required("*Email is required"),
        password: yup.string()
            .required("*Password is required")
    })

    async function handleLogin(values, setSubmitting){
        try {
            await api.login(values.email, values.password);
            history.push("/dashboard");
        } catch(e){
            setSubmitting(false);
            alert(e);
        }
    };

    return (
        <Card className="mx-auto mt-3" style={{maxWidth:"20rem"}}>
            <Card.Body>
                <Card.Title>Login</Card.Title>
                <Formik 
                    initialValues={{email:"", password:""}}
                    validationSchema={validationSchema}
                    onSubmit={(values, { setSubmitting }) => { handleLogin(values, setSubmitting)}}
                      >
                {({
                    values,
                    errors,
                    touched,
                    handleChange,
                    handleBlur,
                    handleSubmit,
                    isSubmitting
                    }) => (<Form onSubmit={handleSubmit}>
                        <Form.Group controlId="f-email">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control type="email" name="email" placeholder="Enter email" value={values.email} onChange={handleChange} onBlur={handleBlur} isInvalid={touched.email && errors.email} />
                            <Form.Control.Feedback type="invalid">
                                {errors.email}
                            </Form.Control.Feedback>
                        </Form.Group>
                        <Form.Group controlId="f-password">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" name="password" placeholder="Password" value={values.password} onChange={handleChange} onBlur={handleBlur} isInvalid={touched.password && errors.password} />
                        </Form.Group>
                        <div>
                        <Button variant="primary" type="submit" disabled={isSubmitting}>
                                        Login
                        </Button>
                        <div style={{display: "inline", float:"right"}} >
                        <Link className="small" to="/forgot-password">forgot password</Link>
                        </div>
                        </div>
                    </Form>)}
                </Formik>
            </Card.Body>
        </Card>
    )
}