import React from "react";
import {useAPI} from "../common/apiContext";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import {Formik} from "formik";
import * as yup from "yup";
import {useHistory, useParams} from "react-router-dom";

export default function ResetPassword() {
    const api = useAPI();
    const {code} = useParams();
    const history = useHistory();

    const validationSchema = yup.object().shape({
        password: yup.string()
            .required("*Password is required")
            .min(10, "Password should be at least 10 characters long")
    })

    async function handleSubmit(values, setSubmitting){
        try {
            await api.resetPassword(values.password, code);
            history.push("/login");
        } catch(e){
            setSubmitting(false);
            alert(e.message);
        }
    };

    return (
        <Card className="mx-auto mt-3" style={{maxWidth:"20rem"}}>
            <Card.Body>
                <Card.Title>Change password</Card.Title>
                <Formik 
                    initialValues={{password:""}}
                    validationSchema={validationSchema}
                    onSubmit={(values, { setSubmitting }) => { handleSubmit(values, setSubmitting)}}
                      >
                {({
                    values,
                    errors,
                    touched,
                    handleChange,
                    handleBlur,
                    handleSubmit,
                    isSubmitting
                    }) => (<Form onSubmit={handleSubmit}>
                        <Form.Group controlId="f-password">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control type="password" name="password" placeholder="At least 10 chars" value={values.password} onChange={handleChange} onBlur={handleBlur} isInvalid={touched.password && errors.password} />
                            <Form.Control.Feedback type="invalid">
                                {errors.password}
                            </Form.Control.Feedback>
                        </Form.Group>
                        <Button variant="primary" type="submit" disabled={isSubmitting}>
                                        Change password
                        </Button>
                    </Form>)}
                </Formik>
            </Card.Body>
        </Card>
    )
}