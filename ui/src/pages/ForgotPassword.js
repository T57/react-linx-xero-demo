import React, {useState} from "react";
import {useAPI} from "../common/apiContext";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import {Formik} from "formik";
import * as yup from "yup";

export default function ForgotPassword() {
    const api = useAPI();
    const [success, setSuccess] = useState(false);

    const validationSchema = yup.object().shape({
        email: yup.string()
            .email("*Must be a valid email address")
            .required("*Email is required")
    })

    async function handleSubmit(values, setSubmitting){
        try {
            await api.recoverPassword(values.email);
            setSuccess(true);
        } catch(e){
            setSubmitting(false);
            alert(e.message);
        }
    };

    function Contents(){
        if(success) {
            return (
            <Card.Text>You will receive an email with a link to reset your password.</Card.Text>
            );
        }
        else {
            return (
                <Formik 
                    initialValues={{email:""}}
                    validationSchema={validationSchema}
                    onSubmit={(values, { setSubmitting }) => { handleSubmit(values, setSubmitting)}}
                      >
                {({
                    values,
                    errors,
                    touched,
                    handleChange,
                    handleBlur,
                    handleSubmit,
                    isSubmitting
                    }) => (<Form onSubmit={handleSubmit}>
                        <Form.Group controlId="f-email">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control type="email" name="email" placeholder="Enter email" value={values.email} onChange={handleChange} onBlur={handleBlur} isInvalid={touched.email && errors.email} />
                            <Form.Control.Feedback type="invalid">
                                {errors.email}
                            </Form.Control.Feedback>
                        </Form.Group>
                        <Button variant="primary" type="submit" disabled={isSubmitting}>
                                        Send password reset email
                        </Button>
                    </Form>)}
                </Formik>
            );
        };
    }

    return (
        <Card className="mx-auto mt-3" style={{maxWidth:"20rem"}}>
            <Card.Body>
                <Card.Title>Forgot password</Card.Title>
                <Contents/>
            </Card.Body>
        </Card>
    )
}