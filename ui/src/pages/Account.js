import React, { useState, useEffect } from "react";
import Form from "react-bootstrap/Form";
import Card from "react-bootstrap/Card";
import {useAPI} from "../common/apiContext";

export default function Account(){
    const api = useAPI();
    const [user, setUser] = useState();

    useEffect(()=>{
        api.getUser()
        .then((u)=>setUser(u))
        .catch((e)=>alert(e));
    },[]);

    return (
        <Card className="mx-auto mt-3" style={{maxWidth:"20rem"}}>
            <Card.Body>
                <Card.Title>Profile</Card.Title>
                <Form>
                    <Form.Group controlId="f-firstName">
                        <Form.Label>First name</Form.Label>
                        <Form.Control type="input" value={user?.FirstName} disabled/>
                    </Form.Group>
                    <Form.Group controlId="f-lastName">
                        <Form.Label>Last name</Form.Label>
                        <Form.Control type="input" value={user?.LastName} disabled/>
                    </Form.Group>
                    <Form.Group controlId="f-email">
                        <Form.Label>Email</Form.Label>
                        <Form.Control type="input" value={user?.Email} disabled/>
                    </Form.Group>
                </Form>
            </Card.Body>
        </Card>
    );
}