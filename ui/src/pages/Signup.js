import React, {useState} from "react";
import {useAPI} from "../common/apiContext";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Card from "react-bootstrap/Card";
import Spinner from "react-bootstrap/Spinner";
import {Formik} from "formik";
import * as yup from "yup";

export default function Signup() {
    const [done, setDone] = useState(false);
    const api = useAPI();

    const validationSchema = yup.object().shape({
        email: yup.string()
            .email("*Must be a valid email address")
            .required("*Email is required"),
        firstName: yup.string()
            .required("*First name is required"),
        lastName: yup.string()
            .required("*Last name is required"),
        password: yup.string()
            .required("*Password is required")
            .min(10, "Password should be at least 10 characters long")
    })

    async function handleSignup(values, setSubmitting){
        try {
            await api.signup(values.firstName, values.lastName, values.email, values.password);
            setDone(true);
        } catch(e){
            alert(e);
            setSubmitting(false);
        };
    };

    const NormalButton = ()=>{
        return (<Button variant="primary" type="submit">Signup</Button>);
    }

    const BusyButton = ()=>{
        return (
        <Button variant="primary" type="submit" disabled>
            <Spinner
                as="span"
                animation="border"
                size="sm"
                role="status"
                aria-hidden="true"
            />
            Signup
        </Button>
        );
    };

    function renderSignupForm() {
        return (
            <Card className="mx-auto mt-3" style={{maxWidth:"20rem"}}>
                <Card.Body>
                    <Card.Title>Signup</Card.Title>
                    <Formik
                        initialValues = {{email:"", firstName:"", lastName:"", password:""}}
                        validationSchema={validationSchema}
                        onSubmit={(values, { setSubmitting }) => { handleSignup(values, setSubmitting)}}
                    >
                    {({
                        values,
                        errors,
                        touched,
                        handleChange,
                        handleBlur,
                        handleSubmit,
                        isSubmitting,
                    }) => (
                    <Form onSubmit={handleSubmit}>
                        <Form.Group controlId="f-email">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control 
                                required 
                                type="email" 
                                name="email"
                                placeholder="Enter email" 
                                value={values.email} 
                                onChange={handleChange}
                                onBlur={handleBlur}
                                isInvalid={touched.email && errors.email}
                            />
                            <Form.Control.Feedback type="invalid">
                                {errors.email}
                            </Form.Control.Feedback>
                        </Form.Group>
                        <Form.Group controlId="f-firstName">
                            <Form.Label>First name</Form.Label>
                            <Form.Control 
                                required
                                type="firstName" 
                                name="firstName"
                                placeholder="Enter first name" 
                                value={values.firstName} 
                                onChange={handleChange}
                                onBlur={handleBlur}
                                isInvalid={touched.firstName && errors.firstName}
                            />
                            <Form.Control.Feedback type="invalid">
                                {errors.firstName}
                            </Form.Control.Feedback>
                        </Form.Group>
                        <Form.Group controlId="f-lastName">
                            <Form.Label>Last name</Form.Label>
                            <Form.Control 
                                required
                                type="lastName" 
                                name="lastName"
                                placeholder="Enter last name" 
                                value={values.lastName} 
                                onChange={handleChange}
                                onBlur={handleBlur}
                                isInvalid={touched.lastName && errors.lastName}
                            />
                            <Form.Control.Feedback type="invalid">
                                {errors.lastName}
                            </Form.Control.Feedback>
                        </Form.Group>
                        <Form.Group controlId="f-password">
                            <Form.Label>Password</Form.Label>
                            <Form.Control 
                                required
                                type="password" 
                                name="password"
                                placeholder="At least 10 chars" 
                                value={values.password} 
                                onChange={handleChange}
                                onBlur={handleBlur}
                                isInvalid={touched.password && errors.password}
                            />
                            <Form.Control.Feedback type="invalid">
                                {errors.password}
                            </Form.Control.Feedback>
                        </Form.Group>
                        {isSubmitting?<BusyButton/>:<NormalButton/>}
                    </Form>
                    )}
                    </Formik>
                </Card.Body>
            </Card>
      );
    };

    

    function renderThankYouForm(){
        return(
            <div >Thanks for signing up. Please check your email for the confirmation link.
            </div>
        )
    };

    return(
        <div>{done ? renderThankYouForm():renderSignupForm()}</div>
    );
}