import React, {useState, useEffect} from 'react';
import {useAPI} from '../common/apiContext';
import {useHistory, useParams} from "react-router-dom";
import Button from "react-bootstrap/Button";
import Jumbotron from "react-bootstrap/Jumbotron";

export default function ConfirmSignup(){
    const [isWaiting, setIsWaiting] = useState(true);
    const [error, setError] = useState();
    const {code} = useParams();
    const history = useHistory();
    const api = useAPI();

    useEffect(() => {
        api.confirmemail(code)
        .then(()=>setIsWaiting(false))
        .catch((e)=>{
            setError(e.message);
            setIsWaiting(false);
        });
    }, [code, history, api]);

    function handleLogin()
    {
        history.push("/login");
    }

    function Contents(){
        if(isWaiting){
            return (
                <p>Confirming code...</p>
            )
        } else if(error != null) {
            return (
                <p>Oops, we have a problem: {error}</p>
            )
        } else {
            return (
                <div>
                    <p>You have successfully confirmed your email address. Please login.</p>
                    <Button onClick={handleLogin}>Login</Button>
                </div>
            );
        };
    }

    return (
        <Jumbotron className="text-center">
            <Contents/>
        </Jumbotron>
    );
}