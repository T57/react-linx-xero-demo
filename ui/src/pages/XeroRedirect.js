import React, {useState, useEffect} from 'react';
import {useAPI} from '../common/apiContext';
import {useHistory} from "react-router-dom";

export default function XeroRedirect(){
    const [error, setError] = useState(null);
    const history = useHistory();
    const api = useAPI();

    useEffect(() => {
        const params = new URLSearchParams(history.location.search);
        api.postXeroConnect(params.get("code"), params.get("state"))
        .then(()=>history.push("/settings"))
        .catch((e)=>setError(e.message));
    }, [history, api]);

    if(error){
        return(
            <div>{error}</div>
        )
    } else {
        return (
        <div>Connecting Xero...</div>
        )
    };
}