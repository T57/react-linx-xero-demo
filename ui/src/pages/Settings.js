import React, {useState, useEffect} from "react";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import {useAPI} from "../common/apiContext";

export default function Settings(){
    const [xeroSettings, setXeroSettings] = useState();
    const api = useAPI();

    useEffect(()=>{
        api.getXeroSettings()
        .then((settings)=>{
            setXeroSettings(settings);
        })
        .catch((e)=>alert(e))
    },[api]);

    function handleConnectToXero(){
        api.getXeroConnect()
        .then((x)=>window.location = x)
        .catch((e)=>alert(e));
    }

    function handleDisconnectFromXero(){
        api.deleteXeroConnect()
        .then(()=>api.getXeroSettings())
        .then((settings)=>setXeroSettings(settings))
        .catch((e)=>alert(e));
    }

    return (
        <Card className="mx-auto mt-3" style={{maxWidth:"30rem"}}>
            <Card.Body>
                <Card.Title>Settings</Card.Title>
                {xeroSettings && xeroSettings.IsConnected &&
                <div>
                    <label>Xero: Connected to {xeroSettings.TenantName}.</label>
                    <Button onClick={handleDisconnectFromXero} variant="outline-secondary">Disconnect from Xero</Button>
                </div>}
                {xeroSettings && !xeroSettings.IsConnected &&
                <div>
                    <label>Your Xero account has not been connected.</label>
                    <Button onClick={handleConnectToXero}>Connect to Xero</Button>
                </div>}
            </Card.Body>
        </Card>
    );
}