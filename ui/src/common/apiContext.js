import React, {useState, createContext, useEffect}  from 'react'

const apiURL = process.env.REACT_APP_API_URI;
const APIContext = createContext();

export const APIContextProvider = (props) => {

    //saving the auth token in localstorage is a XSS risk
    //an http-only, same-site cookie is more secure
    const token = {
            get: ()=>{return localStorage.getItem("t")},
            set: (value)=>{localStorage.setItem("t",value)},
            remove: ()=>{localStorage.removeItem("t")}
    };

    const [user, setUser] = useState(null);
    const [isAuthenticated, setIsAuthenticated] = useState(token.get() != null);

    useEffect(()=>{
        if(isAuthenticated){
            getUser()
            .then((u)=>setUser(u))
            .catch(()=>logout());
        }
    },[]);

    function AuthHeaderValue(){
        return `Bearer ${token.get()}`;
    }

    async function signup(firstName, lastName, email, password){
        const data = {
            FirstName: firstName,
            LastName: lastName,
            Email: email,
            Password: password
        };
        const init = {
            method: "POST",
            headers: {
                "Content-Type":"application/json"
            },
            body: JSON.stringify(data)
        }
        const response = await fetch(`${apiURL}signup`, init);
        if(!response.ok)
            throw Error(response.statusText);
    }

    async function confirmemail(code){
        const init = {
            method: "POST",
            headers: {
                "Content-Type":"application/json"
            },
            body: JSON.stringify(code)
        }
        const response = await fetch(`${apiURL}confirmemail`, init);
        if(!response.ok)
            throw Error(response.statusText);
    }

    async function login(email, password){
        const data = {
            Email: email,
            Password: password
        }
        const init = {
            method: "POST",
            headers: {
                "Content-Type":"application/json"
            },
            body: JSON.stringify(data)
        }
        const response = await fetch(`${apiURL}login`, init);
        if(response.ok){
            const result = await response.json();
            token.set(result.Token);
            setUser(result.User);
            setIsAuthenticated(true);
        }
        else {
            throw Error(response.statusText);
        } 
    }

    async function getUser(){
        const init = {
            method: "GET",
            headers: {
                "Authorization": AuthHeaderValue()
            }
        }
        const response = await fetch(`${apiURL}user`, init);
        if(response.ok){
            return await response.json();
        } else {
            throw Error(response.statusText);
        }
    }

    function logout(){
        token.remove();
        setUser(null);
        setIsAuthenticated(false);
    }

    async function getXeroConnect(){
        const init = {
            method: "GET",
            headers: {
                "Authorization": AuthHeaderValue()
            }
        }
        const response = await fetch(`${apiURL}xero/connect`, init);
        if(response.ok){
            return await response.text();
        } else {
            throw Error(response.statusText);
        }
    }

    async function postXeroConnect(code, state){
        const data = {
            Code: code,
            State: state
        }
        const init = {
            method: "POST",
            headers: {
                "Authorization": AuthHeaderValue(),
                "Content-Type":"application/json"
            },
            body: JSON.stringify(data)
        }
        const response = await fetch(`${apiURL}xero/connect`, init);
        if(!response.ok){
            throw Error(response.statusText);
        }
    }

    async function getXeroSettings(){
        const init = {
            method: "GET",
            headers: {
                "Authorization": AuthHeaderValue()
            }
        }
        const response = await fetch(`${apiURL}xero/settings`, init);
        if(response.ok){
            return await response.json();
        } else {
            throw Error(response.statusText);
        }
    }

    async function deleteXeroConnect(){
        const init = {
            method: "DELETE",
            headers: {
                "Authorization": AuthHeaderValue()
            }
        }
        const response = await fetch(`${apiURL}xero/connect`, init);
        if(!response.ok){
            throw Error(response.statusText);
        }
    }

    async function recoverPassword(email){
        const init = {
            method: "POST",
            headers: {
                "Content-Type":"application/json"
            },
            body: JSON.stringify(email)
        }
        const response = await fetch(`${apiURL}recoverpassword`, init);
        if(!response.ok)
            throw Error(response.statusText);
    }

    async function resetPassword(password, token){
        const data = {
            Token: token,
            Password: password
        }
        const init = {
            method: "POST",
            headers: {
                "Content-Type":"application/json"
            },
            body: JSON.stringify(data)
        }
        const response = await fetch(`${apiURL}resetpassword`, init);
        if(!response.ok){
            throw Error(response.statusText);
        }
    }

    return (
        <APIContext.Provider value={{user, setUser, isAuthenticated, 
            signup, confirmemail, login, getUser, logout, 
            getXeroConnect, postXeroConnect, deleteXeroConnect, getXeroSettings,
            recoverPassword, resetPassword}} {...props} />
    );
};

export const useAPI = () => React.useContext(APIContext);
